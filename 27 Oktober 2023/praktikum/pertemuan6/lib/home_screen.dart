import 'package:flutter/material.dart';

List<String> nama = ["Nama 1", "Nama 2", "Nama 3"];

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: ListView.builder(itemBuilder: (context, index) {
      return Container(
          margin: const EdgeInsets.all(20),
          color: Colors.amber,
          width: MediaQuery.of(context).size.width,
          child: const ListTile(
            leading: Icon(Icons.home),
            title: Text(
              "Praktikum PPB",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text("Pertemuan 6"),
            trailing: Icon(Icons.person),
          ));
    })
        // Container(
        //     margin: const EdgeInsets.all(20),
        //     padding: const EdgeInsets.all(20),
        //     color: Colors.amber,
        //     width: MediaQuery.of(context).size.width,
        //     child: const Row(
        //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //       children: [
        //         Icon(Icons.home),
        //         Column(
        //           children: [
        //             Text(
        //               "Praktikum PPB",
        //               style: TextStyle(fontWeight: FontWeight.bold),
        //             ),
        //             Text("Pertemuan 6")
        //           ],
        //         ),
        //         Icon(Icons.person)
        //       ],
        //     ));

        //  GridView.builder(
        //     gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        //         crossAxisCount: 3),
        //     itemCount: 10,
        //     itemBuilder: (context, index) {
        //       return Container(
        //         margin: const EdgeInsets.all(20),
        //         color: Colors.amber,
        //         height: 50,
        //         width: MediaQuery.of(context).size.width,
        //         child: Text("Index ke-$index"),
        //       );
        //     })

        // ListView.builder(
        //     itemCount: 3,
        //     itemBuilder: (context, index) {
        //       return Container(
        //           margin: const EdgeInsets.all(20),
        //           color: Colors.amber,
        //           height: 50,
        //           width: MediaQuery.of(context).size.width,
        //           child: Text(nama[index]));
        //     })

        // ListView(
        //   children: [
        //     Container(
        //         margin: const EdgeInsets.all(20),
        //         color: Colors.amber,
        //         height: 50,
        //         width: MediaQuery.of(context).size.width),
        //   ],
        // ),
        );
  }
}
